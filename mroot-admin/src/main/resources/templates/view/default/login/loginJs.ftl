<#--表单验证国际化提示信息-->
<script>

    /**
     * 登录验证信息
     */
    var LoginValidation = function () {


        var username_required = '${I18N("login.username.required")}';
        var username_cnLetterNumUl5To18 = '${I18N("login.username.cnLetterNumUl5To18")}';


        var password_required = '${I18N("login.password.required")}';
        var password_letterNum6To18 = '${I18N("login.password.letterNum6To18")}';

        var verify_code_required = '${I18N("login.verifyCode.required")}';


        return {

            getUsernameRequired: function () {
                return username_required;
            },

            // -------------------------------------------------------------------------------------------------

            getUsernameCnLetterNumUl5To18: function () {
                return username_cnLetterNumUl5To18;
            },

            // -------------------------------------------------------------------------------------------------

            getPasswordRequired: function () {
                return password_required;
            },

            // -------------------------------------------------------------------------------------------------

            getPasswordLetterNum6To18: function () {
                return password_letterNum6To18;
            },

            // -------------------------------------------------------------------------------------------------

            getVerifyCodeRequired: function () {
                return verify_code_required;
            }

            // -------------------------------------------------------------------------------------------------

        }
    }();

    // ---------------------------------------------------------------------------------------------------------

</script>
