var FormValidation = function () {

    /**
     * ajax 请求
     * @param form
     */
    var ajaxSubmit = function (form) {
        $('button:submit').addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
        //表单提交
        var url = form.get(0).action;
        $.ajax({
            url: url,
            method: 'POST',
            dataType: 'JSON',
            data: form.serialize(),
            success: function (result) {
                $('button:submit').removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                $('#formToken').val(result.formToken);
                if (AlertMessage.getMessageAlertAjaxOk() === result.state) {
                    Tool.showSweetAlertSuccess({
                        title: result.message,
                        isSkip: true
                    });
                } else if (AlertMessage.getMessageAlertAjaxFail() === result.state) {
                    if (AlertMessage.getMessageAlertAjaxValidation() === result.validation) {
                        showFormMsg(result.message);
                    } else {
                        Tool.showSweetAlertError({
                            title: result.message
                        });
                    }
                } else if (AlertMessage.getMessageAlertAjaxRepeat() === result.state) {
                    Tool.showSweetAlertWarning({
                        title: result.message
                    });
                } else if (AlertMessage.getMessageAlertAjaxError() === result.state) {
                    Tool.showSweetAlertError({
                        title: result.message
                    });
                } else {
                    Tool.showSweetAlertError({
                        title: result.message
                    });
                }
            },
            error: function () {
                $('button:submit').removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                Tool.showSweetAlertError({
                    title: AlertMessage.getMessageAlertNetworkError()
                });
            }
        });

    };

    // -------------------------------------------------------------------------------------------------

    /**
     * 显示 form_msg
     */
    var showFormMsg = function (text) {
        var alert = $('#form_msg');
        if (text) {
            $('#alert_text').html(text);
        }
        alert.removeClass('m--hide').show();
        mApp.scrollTo(alert, -200);
    };

    // -------------------------------------------------------------------------------------------------

    /**
     * 隐藏 form_msg
     */
    var hideFormMsg = function () {
        var alert = $('#form_msg');
        alert.addClass('m--hide').hide();
    };

    // -------------------------------------------------------------------------------------------------

    return {

        init: function () {

        },

        /*代码生成表单验证*/
        formValidationGenerate: function () {

            $('#generate_form').validate({
                rules: {},
                messages: {},
                invalidHandler: function (event, validator) {
                    showFormMsg();
                },
                submitHandler: function (form) {
                    //$('button:submit', form).prop('disabled', true);
                    //form[0].submit();

                    hideFormMsg();

                    form = $('#generate_form');
                    ajaxSubmit(form);
                }
            });

            // -------------------------------------------------------------------------------------------------

        },

        // -------------------------------------------------------------------------------------------------

        /*用户表单验证*/
        formValidationUser: function () {

            $('#user_form').validate({
                rules: {
                    type: {
                        required: true
                    },
                    username: {
                        cnLetterNumUl5To18: true
                    },
                    nickName: {
                        cnLetterNumUl0To18: true
                    },
                    password: {
                        letterNum0To18: true
                    },
                    email: {
                        emailNullable: true
                    },
                    phone: {
                        phoneNum: true
                    },
                    realName: {
                        cn0To18: true
                    }
                },
                messages: {
                    type: {
                        required: UserValidation.getTypeRequired()
                    },
                    username: {
                        cnLetterNumUl5To18: UserValidation.getUsernameCnLetterNumUl5To18()
                    },
                    nickName: {
                        cnLetterNumUl0To18: UserValidation.getNickNameCnLetterNumUl0To18()
                    },
                    password: {
                        letterNum0To18: UserValidation.getPasswordLetterNum6To18()
                    },
                    email: {
                        emailNullable: UserValidation.getEmail()
                    },
                    phone: {
                        phoneNum: UserValidation.getPhonePhoneNum()
                    },
                    realName: {
                        cn0To18: UserValidation.getRealNameCn0To18()
                    }
                },
                invalidHandler: function (event, validator) {
                    showFormMsg();
                },
                submitHandler: function (form) {
                    //$('button:submit', form).prop('disabled', true);
                    //form[0].submit();
                    hideFormMsg();

                    form = $('#user_form');
                    ajaxSubmit(form);
                }
            });

            // -------------------------------------------------------------------------------------------------

        },

        // -------------------------------------------------------------------------------------------------

        /*昵称表单验证*/
        formValidationNickname: function () {

            $('#nickName_form').validate({
                rules: {
                    nickName: {
                        cnLetterNumUl2To18: true
                    },
                    password: {
                        letterNum6To18: true
                    }
                },
                messages: {
                    nickName: {
                        cnLetterNumUl2To18: UserValidation.getPasswordLetterNum6To18()
                    },
                    password: {
                        letterNum6To18: UserValidation.getPasswordLetterNum6To18()
                    }
                },
                invalidHandler: function (event, validator) {
                    showFormMsg();
                },
                submitHandler: function (form) {
                    //$('button:submit', form).prop('disabled', true);
                    //form[0].submit();

                    hideFormMsg();

                    form = $('#nickName_form');
                    ajaxSubmit(form);
                }
            });

            // ------------------------------------------------------------------------
        },

        // -------------------------------------------------------------------------------------------------

        /*密码表单验证*/
        formValidationPassword: function () {

            $('#password_form').validate({
                rules: {
                    oldPassword: {
                        letterNum6To18: true
                    },
                    password: {
                        letterNum6To18: true
                    },
                    confirmPassword: {
                        equalTo: '#password'
                    }
                },
                messages: {
                    oldPassword: {
                        letterNum6To18: UserValidation.getPasswordLetterNum6To18()
                    },
                    password: {
                        letterNum6To18: UserValidation.getPasswordLetterNum6To18()
                    },
                    confirmPassword: {
                        equalTo: UserValidation.getConfirmPassword()
                    }
                },
                invalidHandler: function (event, validator) {
                    showFormMsg();
                },
                submitHandler: function (form) {
                    //$('button:submit', form).prop('disabled', true);
                    //form[0].submit();
                    hideFormMsg();

                    form = $('#password_form');
                    ajaxSubmit(form);
                }
            });

            // ------------------------------------------------------------------------

        },

        // -------------------------------------------------------------------------------------------------

        /*用户授权表单验证*/
        formValidationUserAuthorization: function () {

            $('#user_authorization_form').validate({
                rules: {},
                messages: {},
                invalidHandler: function (event, validator) {
                    showFormMsg();
                },
                submitHandler: function (form) {
                    //$('button:submit', form).prop('disabled', true);
                    //form[0].submit();
                    hideFormMsg();

                    form = $('#user_authorization_form');
                    ajaxSubmit(form);
                }

            });

            // ------------------------------------------------------------------------

        },

        // -------------------------------------------------------------------------------------------------

        /*角色表单验证*/
        formValidationRole: function () {

            $('#role_form').validate({
                rules: {
                    name: {
                        letterNumUl2To80: true
                    },
                    title: {
                        letterNumUl2To80: true
                    },
                    sort: {
                        zeroPositiveInteger: true
                    },
                    remark: {
                        cnLetterNumUlSymbol0To200: true
                    }
                },
                messages: {
                    name: {
                        letterNumUl2To80: RoleValidation.getNamePattern()
                    },
                    title: {
                        letterNumUl2To80: RoleValidation.getTitlePattern()
                    },
                    sort: {
                        zeroPositiveInteger: RoleValidation.getSortRange()
                    },
                    remark: {
                        cnLetterNumUlSymbol0To200: RoleValidation.getRemarkPattern()
                    }
                },
                invalidHandler: function (event, validator) {
                    showFormMsg();
                },
                submitHandler: function (form) {
                    //$('button:submit', form).prop('disabled', true);
                    //form[0].submit();
                    hideFormMsg();

                    form = $('#role_form');
                    ajaxSubmit(form);
                }

            });

            // ------------------------------------------------------------------------

        },

        // -------------------------------------------------------------------------------------------------

        /*角色授权表单验证*/
        formValidationRoleAuthorization: function () {

            $('#role_authorization_form').validate({
                rules: {},
                messages: {},
                invalidHandler: function (event, validator) {
                    showFormMsg();
                },
                submitHandler: function (form) {
                    //$('button:submit', form).prop('disabled', true);
                    //form[0].submit();
                    hideFormMsg();

                    var ref = $('#rule_tree').jstree(true);
                    sel = ref.get_selected();

                    $('#rules').val(sel);

                    form = $('#role_authorization_form');
                    ajaxSubmit(form);
                }

            });

            // ------------------------------------------------------------------------

        },

        // -------------------------------------------------------------------------------------------------

        /*权限表单验证*/
        formValidationRule: function () {

            $('#rule_form').validate({
                rules: {
                    pid: {
                        zeroPositiveInteger: true
                    },
                    type: {
                        positiveInteger: true
                    },
                    name: {
                        letterNumUl2To80: true
                    },
                    title: {
                        letterNumUl2To80: true
                    },
                    url: {
                        letterNumUlBias1To255: true
                    },
                    sort: {
                        zeroPositiveInteger: true
                    },
                    remark: {
                        cnLetterNumUlSymbol0To200: true
                    }
                },
                messages: {
                    pid: {
                        zeroPositiveInteger: RuleValidation.getPidRange()
                    },
                    type: {
                        positiveInteger: RuleValidation.getTypeRange()
                    },
                    name: {
                        letterNumUl2To80: RuleValidation.getNamePattern()
                    },
                    title: {
                        letterNumUl2To80: RuleValidation.getTitlePattern()
                    },
                    url: {
                        letterNumUlBias1To255: RuleValidation.getUrlPattern()
                    },
                    sort: {
                        zeroPositiveInteger: RuleValidation.getSortRange()
                    },
                    remark: {
                        cnLetterNumUlSymbol0To200: RuleValidation.getRemarkPattern()
                    }
                },
                invalidHandler: function (event, validator) {
                    showFormMsg();
                },
                submitHandler: function (form) {
                    //$('button:submit', form).prop('disabled', true);
                    //form[0].submit();
                    hideFormMsg();

                    form = $('#rule_form');
                    ajaxSubmit(form);
                }
            });

            // -------------------------------------------------------------------------------------------------

        },

        // -------------------------------------------------------------------------------------------------

        /*系统配置表单验证*/
        formValidationConfig: function () {

            $('#config_form').validate({
                rules: {
                    type: {
                        positiveInteger: true
                    },
                    name: {
                        letterNumUl2To80: true
                    },
                    value: {
                        letterNumUlSymbol1To255: true
                    },
                    title: {
                        letterNumUl2To80: true
                    },
                    sort: {
                        zeroPositiveInteger: true
                    },
                    remark: {
                        cnLetterNumUlSymbol0To200: true
                    }
                },
                messages: {
                    type: {
                        positiveInteger: ConfigValidation.getTypeRange()
                    },
                    name: {
                        letterNumUl2To80: ConfigValidation.getNamePattern()
                    },
                    title: {
                        letterNumUl2To80: ConfigValidation.getTitlePattern()
                    },
                    value: {
                        letterNumUlSymbol1To255: ConfigValidation.getValuePattern()
                    },
                    sort: {
                        zeroPositiveInteger: ConfigValidation.getSortRange()
                    },
                    remark: {
                        cnLetterNumUlSymbol0To200: ConfigValidation.getRemarkPattern()
                    }
                },
                invalidHandler: function (event, validator) {
                    showFormMsg();
                },
                submitHandler: function (form) {
                    //$('button:submit', form).prop('disabled', true);
                    //form[0].submit();
                    hideFormMsg();

                    form = $('#config_form');
                    ajaxSubmit(form);
                }
            });

            // -------------------------------------------------------------------------------------------------

        },

        // -------------------------------------------------------------------------------------------------

        /*文章分类表单验证*/
        formValidationCategory: function () {

            $('#category_form').validate({
                rules: {
                    pid: {
                        zeroPositiveInteger: true
                    },
                    type: {
                        positiveInteger: true
                    },
                    name: {
                        letterNumUl2To80: true
                    },
                    title: {
                        letterNumUl2To80: true
                    },
                    sort: {
                        zeroPositiveInteger: true
                    },
                    remark: {
                        cnLetterNumUlSymbol0To200: true
                    }
                },
                messages: {
                    pid: {
                        zeroPositiveInteger: CategoryValidation.getPidRange()
                    },
                    type: {
                        positiveInteger: CategoryValidation.getTypeRange()
                    },
                    name: {
                        letterNumUl2To80: CategoryValidation.getNamePattern()
                    },
                    title: {
                        letterNumUl2To80: CategoryValidation.getTitlePattern()
                    },
                    sort: {
                        zeroPositiveInteger: CategoryValidation.getSortRange()
                    },
                    remark: {
                        cnLetterNumUlSymbol0To200: CategoryValidation.getRemarkPattern()
                    }
                },
                invalidHandler: function (event, validator) {
                    showFormMsg();
                },
                submitHandler: function (form) {
                    //$('button:submit', form).prop('disabled', true);
                    //form[0].submit();

                    hideFormMsg();

                    form = $('#category_form');
                    ajaxSubmit(form);
                }
            });

            // -------------------------------------------------------------------------------------------------

        },

        // -------------------------------------------------------------------------------------------------

        /*文章表单验证*/
        formValidationArticle: function () {

            $('#article_form').validate({
                rules: {
                    categoryId: {
                        positiveInteger: true
                    },
                    title: {
                        required: true
                    },
                    description: {
                        required: true
                    }
                },
                messages: {
                    categoryId: {
                        positiveInteger: ArticleValidation.getCategoryIdPattern()
                    },
                    title: {
                        required: ArticleValidation.getTitlePattern()
                    },
                    description: {
                        required: ArticleValidation.getDescriptionPattern()
                    }
                },
                invalidHandler: function (event, validator) {
                    showFormMsg();
                },
                submitHandler: function (form) {
                    //$('button:submit', form).prop('disabled', true);
                    //form[0].submit();

                    if (UE.getEditor('articleContent').hasContents()) {
                        hideFormMsg();
                        form = $('#article_form');
                        ajaxSubmit(form);
                    } else {
                        showFormMsg(ArticleValidation.getContentPattern());
                    }
                }
            });

            // -------------------------------------------------------------------------------------------------

        },

        // -------------------------------------------------------------------------------------------------

        /*系统配置表单验证*/
        formValidationScheduleJob: function () {

            $('#scheduleJob_form').validate({
                rules: {
                    name: {
                        letterNumUl2To80: true
                    },
                    title: {
                        letterNumUl2To80: true
                    },
                    beanName: {
                        letterNum2To100: true
                    },
                    methodName: {
                        letterNum1To100: true
                    },
                    params: {
                        maxlength: 255
                    },
                    cronExpression: {
                        required: 100
                    },
                    sort: {
                        zeroPositiveInteger: true
                    },
                    remark: {
                        cnLetterNumUlSymbol0To200: 200
                    }
                },
                messages: {
                    name: {
                        letterNumUl2To80: ScheduleJobValidation.getNamePattern()
                    },
                    title: {
                        letterNumUl2To80: ScheduleJobValidation.getTitlePattern()
                    },
                    beanName: {
                        letterNum2To100: ScheduleJobValidation.getBeanNamePattern()
                    },
                    methodName: {
                        letterNum1To100: ScheduleJobValidation.getMethodNamePattern()
                    },
                    params: {
                        maxlength: ScheduleJobValidation.getParamsPattern()
                    },
                    cronExpression: {
                        required: ScheduleJobValidation.getCronExpressionPattern()
                    },
                    sort: {
                        zeroPositiveInteger: ScheduleJobValidation.getSortRange()
                    },
                    remark: {
                        cnLetterNumUlSymbol0To200: ScheduleJobValidation.getRemarkPattern()
                    }
                },
                invalidHandler: function (event, validator) {
                    showFormMsg();
                },
                submitHandler: function (form) {
                    //$('button:submit', form).prop('disabled', true);
                    //form[0].submit();
                    hideFormMsg();
                    form = $('#scheduleJob_form');
                    ajaxSubmit(form);
                }

            });

            // -------------------------------------------------------------------------------------------------

        }

        // -------------------------------------------------------------------------------------------------

    };

}();
