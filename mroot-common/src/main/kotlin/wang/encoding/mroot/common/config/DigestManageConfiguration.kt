/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.config


import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.EnableCaching
import org.springframework.stereotype.Component
import wang.encoding.mroot.common.constant.SecurityConst
import wang.encoding.mroot.common.util.DigestUtil


/**
 * 摘要加密工具类(非可逆加密)
 *
 * @author ErYang
 */
@Component
@EnableCaching
class DigestManageConfiguration {

    @Autowired
    private lateinit var securityConst: SecurityConst


    /***
     * 利用 DigestUtil 的工具类实现 SHA-512 加密
     * @param data 需要加密的字符串
     * @return 加密后的报文
     */
    fun getSha512(data: String): String {
        // 加密盐
        val stringBuffer = StringBuffer(securityConst.digestKey)
        stringBuffer.append(data)
        return DigestUtil.getSha512(stringBuffer.toString())
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End DigestManageConfiguration class

/* End of file DigestManageConfiguration.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/common/config/DigestManageConfiguration.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
