/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.config


import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.MessageSource
import org.springframework.context.i18n.LocaleContextHolder
import org.springframework.stereotype.Component
import java.util.*


/**
 * 国际化信息工具类 业务使用
 *
 * @author ErYang
 */
@Component
@EnableCaching
class LocaleMessageSourceConfiguration {

    @Autowired
    private lateinit var messageSource: MessageSource

    // -------------------------------------------------------------------------------------------------


    /**
     * 获取 message 信息
     *
     * @param code           对应 message 配置的key
     * @param args           数组参数
     * @param defaultMessage 没有设置key的时候的默认值
     * @return String
     */
    @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    fun getMessage(code: String, args: Array<Any>? = null, defaultMessage: String = code): String {
        // 这里使用比较方便的方法，不依赖 request
        val locale: Locale = LocaleContextHolder.getLocale()
        return messageSource.getMessage(code, args, defaultMessage, locale)
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End LocaleMessageSourceConfiguration class

/* End of file LocaleMessageSourceConfiguration.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/common/config/LocaleMessageSourceConfiguration.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
