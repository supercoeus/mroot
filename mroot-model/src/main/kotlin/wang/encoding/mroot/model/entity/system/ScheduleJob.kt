/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]
<http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>
<http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.model.entity.system


import com.baomidou.mybatisplus.activerecord.Model
import com.baomidou.mybatisplus.annotations.TableId
import com.baomidou.mybatisplus.annotations.TableName
import com.baomidou.mybatisplus.enums.IdType
import org.hibernate.validator.constraints.Length
import org.hibernate.validator.constraints.Range

import java.math.BigInteger
import java.util.*
import javax.validation.constraints.NotNull
import javax.validation.constraints.Past
import javax.validation.constraints.Pattern
import java.io.Serializable


/**
 * 定时任务实体类
 *
 * @author ErYang
 */
@TableName("system_schedule_job")
class ScheduleJob : Model<ScheduleJob>(), Serializable {

    companion object {

        private const val serialVersionUID = -2812481718178613893L

        /* 属性名称常量开始 */

        // -------------------------------------------------------------------------------------------------

        /**
         * 任务ID
         */
        const val ID: String = "id"

        /**
         * 状态(1是正常，2是禁用，3是删除)
         */
        const val STATUS: String = "status"

        /**
         * 名称
         */
        const val TITLE: String = "title"

        /**
         * 标识
         */
        const val NAME: String = "name"

        // -------------------------------------------------------------------------------------------------

        /* 属性名称常量结束 */

        /**
         * 现有的对象赋值给一个新的对象
         *
         * @param scheduleJob  ScheduleJob
         * @return ScheduleJob
         */
        fun copy2New(scheduleJob: ScheduleJob): ScheduleJob {
            val newScheduleJob = ScheduleJob()
            newScheduleJob.beanName = scheduleJob.beanName
            newScheduleJob.cronExpression = scheduleJob.cronExpression
            newScheduleJob.gmtCreate = scheduleJob.gmtCreate
            newScheduleJob.gmtCreateIp = scheduleJob.gmtCreateIp
            newScheduleJob.gmtModified = scheduleJob.gmtModified
            newScheduleJob.id = scheduleJob.id
            newScheduleJob.methodName = scheduleJob.methodName
            newScheduleJob.name = scheduleJob.name
            newScheduleJob.params = scheduleJob.params
            newScheduleJob.remark = scheduleJob.remark
            newScheduleJob.sort = scheduleJob.sort
            newScheduleJob.status = scheduleJob.status
            newScheduleJob.title = scheduleJob.title
            return newScheduleJob
        }

        // -------------------------------------------------------------------------------------------------

    }


    /**
     * 任务ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    var id: BigInteger? = null

    // -------------------------------------------------------------------------------------------------

    /**
     * Spring Bean名称
     */
    @Pattern(regexp = "^[a-zA-Z0-9]{1,100}$", message = "validation.system.scheduleJob.beanName.pattern")
    var beanName: String? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * cron表达式
     */
    @Length(min = 1, max = 100, message = "validation.system.scheduleJob.cronExpression.pattern")
    var cronExpression: String? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 创建时间
     */
    @Past(message = "validation.gmtCreate.past")
    var gmtCreate: Date? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 创建IP
     */
    @Pattern(regexp = "^[0-9.]{6,50}\$", message = "validation.gmtCreateIp.pattern")
    var gmtCreateIp: String? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 修改时间
     */
    @Past(message = "validation.gmtModified.past")
    var gmtModified: Date? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 方法名
     */
    @Pattern(regexp = "^[a-zA-Z0-9]{1,100}$", message = "validation.system.scheduleJob.methodName.pattern")
    var methodName: String? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 标识
     */
    @Pattern(regexp = "^[a-zA-Z0-9_\\u4e00-\\u9fa5]{2,80}$", message = "validation.name.pattern")
    var name: String? = null


    // -------------------------------------------------------------------------------------------------

    /**
     * 参数
     */
    @Pattern(regexp = "^[a-zA-Z0-9]{0,255}$", message = "validation.system.scheduleJob.params.pattern")
    var params: String? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 备注
     */
    @Pattern(regexp = "^[a-zA-Z0-9，。、\\u4e00-\\u9fa5]{0,200}$", message = "validation.remark.pattern")
    var remark: String? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 排序
     */
    @Range(min = 0, message = "validation.sort.range")
    var sort: Long? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 状态(1是正常，2是禁用，3是删除)
     */
    @NotNull(message = "validation.status.range")
    @Range(min = 1, max = 3, message = "validation.status.range")
    var status: Int? = null

    // -------------------------------------------------------------------------------------------------

    /**
     * 名称
     */
    @Pattern(regexp = "^[a-zA-Z0-9_\\u4e00-\\u9fa5]{2,80}$", message = "validation.title.pattern")
    var title: String? = null


    // -------------------------------------------------------------------------------------------------

    override fun pkVal(): BigInteger? {
        return this.id
    }

    // -------------------------------------------------------------------------------------------------

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ScheduleJob

        if (id != other.id) return false
        if (beanName != other.beanName) return false
        if (cronExpression != other.cronExpression) return false
        if (gmtCreate != other.gmtCreate) return false
        if (gmtCreateIp != other.gmtCreateIp) return false
        if (gmtModified != other.gmtModified) return false
        if (methodName != other.methodName) return false
        if (name != other.name) return false
        if (params != other.params) return false
        if (remark != other.remark) return false
        if (sort != other.sort) return false
        if (status != other.status) return false
        if (title != other.title) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + (beanName?.hashCode() ?: 0)
        result = 31 * result + (cronExpression?.hashCode() ?: 0)
        result = 31 * result + (gmtCreate?.hashCode() ?: 0)
        result = 31 * result + (gmtCreateIp?.hashCode() ?: 0)
        result = 31 * result + (gmtModified?.hashCode() ?: 0)
        result = 31 * result + (methodName?.hashCode() ?: 0)
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + (params?.hashCode() ?: 0)
        result = 31 * result + (remark?.hashCode() ?: 0)
        result = 31 * result + (sort?.hashCode() ?: 0)
        result = 31 * result + (status ?: 0)
        result = 31 * result + (title?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "ScheduleJob(id=$id, beanName=$beanName, cronExpression=$cronExpression, gmtCreate=$gmtCreate, gmtCreateIp=$gmtCreateIp, gmtModified=$gmtModified, methodName=$methodName, name=$name, params=$params, remark=$remark, sort=$sort, status=$status, title=$title)"
    }


    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ScheduleJob class

/* End of file ScheduleJob.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/model/entity/system/ScheduleJob.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
